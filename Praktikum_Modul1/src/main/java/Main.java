
public class Main {

    public static void main(String[] args) {

        UKM ukm = new UKM();
        Penduduk[]anggota;
        anggota = new Penduduk [10];
        int n = 0;
        
        System.out.println("Unit Kegiatan Mahasiswa ");
        System.out.println("-----------------------------------------------------------");
        
        Mahasiswa ketua = new Mahasiswa ();
        ketua.setNama("Rudi Pratama");
        ketua.setNim(1899203);
        ketua.setTanggalLahir("17 mei 1999");

        Mahasiswa sekre = new Mahasiswa ();
        sekre.setNama("Maudy Leonnardo");
        sekre.setNim(1849501);
        sekre.setTanggalLahir("13 Maret 2000");
        
        ukm.setKetua(ketua);
        ukm.setSekretaris(sekre);
        ukm.setNamaUnit("Acara");
        System.out.println("" +ukm.getNamaUnit());
        System.out.println("-------------------------------------");
        System.out.println("Ketua");
        System.out.println("Ketua\t\t : " +ukm.getKetua().getNama());
        System.out.println("NIM\t\t : " +ketua.getNim());
        System.out.println("Tanggal Lahir\t : "+ketua.getTanggalLah());
        System.out.println("");
        System.out.println("Sekertaris");
        System.out.println("Sekretaris\t : " +ukm.getSekretaris().getNama());
        System.out.println("NIM\t\t : " +sekre.getNim());
        System.out.println("Tanggal Lahir\t : "+sekre.getTanggalLah());
        System.out.println("-------------------------------------");
        System.out.println("");
        anggota[n] = new MasyarakatSekitar();
        ((MasyarakatSekitar)anggota[n]).setNama("Rachel Lim");
        ((MasyarakatSekitar)anggota[n]).setNomor(195);
        ((MasyarakatSekitar)anggota[n]).setTanggalLahir("5 Januari 2001");
        n++;
        
        anggota[n] = new Mahasiswa();
        ((Mahasiswa)anggota[n]).setNama("Nasya soekarno");
        ((Mahasiswa)anggota[n]).setNim(185324135);
        ((Mahasiswa)anggota[n]).setTanggalLahir("17 Desember 2001");
        n++;

    
        System.out.println("Anggota");
        for (int i = 0; i < n; i++) {
            if(anggota[i]instanceof Mahasiswa){
                System.out.println("Mahasiswa");
                System.out.println("Nama\t\t: "+anggota[i].getNama());
                System.out.println("NIM\t\t: "+((Mahasiswa)anggota[i]).getNim());
                System.out.println("Tanggal Lahir\t: "+anggota[i].getTanggalLah());
                System.out.println("Iuran\t\t: Rp"+((Mahasiswa)anggota[i]).hitungIuran());  
                System.out.println("");
            }
            else {
                System.out.println("Masyarakat Sekitar");
                 System.out.println("Nama\t\t: "+anggota[i].getNama());
                System.out.println("NIM\t\t: "+((MasyarakatSekitar)anggota[i]).getNomor());
                System.out.println("Tanggal Lahir\t: "+anggota[i].getTanggalLah());
                System.out.println("Iuran\t\t: Rp"+((MasyarakatSekitar)anggota[i]).hitungIuran());  
            
            }
        }
    
    }
}
